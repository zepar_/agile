package com.fhws.agile.data.content;

public class Content {
	private String name;
	private String id;
	private ContentType type;

	public String getName() {
		return name;
	}

	public String getId() {
		return id;
	}

	public ContentType getType() {
		return type;
	}

	public Content(String name, String id, ContentType type) {
		this.name = name;
		this.id = id;
		this.type = type;
	}

	public enum ContentType{
		PDF,
		VIDEO
	}
}
