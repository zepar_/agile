package com.fhws.agile.data;

import android.content.Context;
import android.content.SharedPreferences;

public class Permanence {

	private  SharedPreferences preferences;

	private final String PREFERENCE_KEY = "fhws.student.agile.PREFERENCES";

	private final String LEVEL_KEY = "LEVEL";
	private final String EXPERIENCE_KEY = "EXPERIENCE";
	private final String PROGRESS_KEY = "PROGRESS";

	public Permanence(Context context) {
		this.preferences = context.getSharedPreferences(PREFERENCE_KEY, Context.MODE_PRIVATE);
	}

	public void putLevel(int level) {
		this.preferences.edit().putInt(LEVEL_KEY, level).apply();
	}

	public int getLevel() {
		return this.preferences.getInt(LEVEL_KEY, 0);
	}

	public void putExperience(int experience) {
		this.preferences.edit().putInt(EXPERIENCE_KEY, experience).apply();
	}

	public int getExperience() {
		return this.preferences.getInt(EXPERIENCE_KEY, 0);
	}

	public void putProgress(int progress) {
		this.preferences.edit().putInt(PROGRESS_KEY, progress).apply();
	}

	public int getProgress() {
		return this.preferences.getInt(PROGRESS_KEY, 0);
	}

	public void RESET() {
		this.preferences.edit().clear().apply();
	}
}
