package com.fhws.agile.data.quiz;

import android.content.Context;
import android.content.res.AssetManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Quiz {
	private Question[] questions;
	private int currentQuestion;
	private int correctAnswers;

	public Quiz(Question[] questions) {
		this.questions = questions;

		this.currentQuestion = 0;
		this.correctAnswers = 0;
	}

	public Quiz(String id, Context context) {
		AssetManager assetManager = context.getAssets();

		String data = null;


		try {
			InputStream inputStream = assetManager.open("quiz/"+ id + ".json");
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

			StringBuffer rawData = new StringBuffer();
			String line = "";

			int times = 0;
			while(true) {
				line = bufferedReader.readLine();
				if(line != null) {
					rawData.append(line + "\n");
					times++;
				} else break;
			}

			data = rawData.toString();

			bufferedReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			JSONArray rawQuestions = new JSONArray(data);

			this.questions = new Question[rawQuestions.length()];

			for(int i = 0; i < rawQuestions.length(); i++) {
				JSONObject currentQuestion = (JSONObject) rawQuestions.get(i);

				JSONArray rawAnswers = currentQuestion.getJSONArray("answers");
				JSONArray rawCorrectAnswers = currentQuestion.getJSONArray("correctAnswers");

				String[] answers = new String[rawAnswers.length()];
				for(int j = 0; j < answers.length; j++) {
					answers[j] = rawAnswers.getString(j);
				}

				int[] correctAnswers = new int[rawCorrectAnswers.length()];
				for(int j = 0; j < correctAnswers.length; j++) {
					correctAnswers[j] = rawCorrectAnswers.getInt(j);
				}

				Question.QuestionType type = currentQuestion.getInt("questionType") == 0 ? Question.QuestionType.SINGLE_ANSWER : Question.QuestionType.MULTIPLE_ANSWERS;

				Question question = new Question(
						currentQuestion.getString("question"),
						answers,
						correctAnswers,
						type
				);

				this.questions[i] = question;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		this.currentQuestion = 0;
		this.correctAnswers = 0;
	}

	public Question getCurrentQuestion() {
		return this.questions[currentQuestion];
	}

	public boolean checkAnswer(int[] answers) {
		if(this.questions[currentQuestion].getSolutions().length == 0) {
			this.correctAnswers++;
			return true;
		}
		if(Arrays.equals(questions[currentQuestion].getSolutions(), answers)) {
			this.correctAnswers++;
			return true;
		} else {
			return false;
		}
	}

	public boolean nextQuestion() {
		currentQuestion++;

		if(currentQuestion == questions.length) {
			currentQuestion--;
			return false;
		}

		return true;
	}
}
