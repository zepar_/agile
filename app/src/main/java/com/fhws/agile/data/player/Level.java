package com.fhws.agile.data.player;

public class Level {
	private int level;
	private int currentExperience;
	private int maxExperience;

	public Level(int initLevel, int initExperience) {
		this.level = initLevel;
		this.currentExperience = initExperience;
		this.maxExperience = LevelTable.values()[initLevel].getMaxXP();
	}

	public void addXP(int amount) {
		currentExperience += amount;

		if(currentExperience >= maxExperience) {
			currentExperience = maxExperience - currentExperience;

			if(level + 1 != LevelTable.values().length) {
				level++;
				maxExperience = LevelTable.values()[level].getMaxXP();
			}
		}
	}

	public int getExperience() {
		return this.currentExperience;
	}

	public int getMaxExperience() {
		return this.maxExperience;
	}

	public int getLevel() {
		return this.level + 1;
	}

	private enum LevelTable {
		One(100),
		Two(150),
		Three(250),
		Four(300);

		private int experience;

		LevelTable(int experience) {
			this.experience = experience;
		}

		public int getMaxXP() {
			return this.experience;
		}
	}
}
