package com.fhws.agile.data.player;

import android.content.Context;

import com.fhws.agile.data.Permanence;

public class Player {
	private Level level;
	private Progress progress;

	private Permanence permanence;

	private static Player self;

	private Player(Context context) {
		this.permanence = new Permanence(context);

		this.level = new Level(permanence.getLevel(), permanence.getExperience());
		this.progress = new Progress(permanence.getProgress(), context);
	}

	public static Player getPlayer(Context context) {
		if(Player.self == null) {
			Player.self = new Player(context);
		}
		return Player.self;
	}

	public void giveXP(int amount) {
		this.level.addXP(amount);

		this.permanence.putExperience(this.level.getExperience());
		this.permanence.putLevel(this.level.getLevel() - 1);
	}

	public int getCurrentProgress() {
		return this.progress.getProgress();
	}

	public void addProgress() {
		this.progress.addProgress();

		this.permanence.putProgress(this.progress.getProgress());
	}

	public Progress getProgress() {
		return this.progress;
	}

	public void setProgress(int i) {
		this.progress.setProgress(i);

		this.permanence.putProgress(i);
	}

	public int getCurrentLevel() {
		return this.level.getLevel();
	}

	public int getExperience() {
		return this.level.getExperience();
	}

	public int getMaxExperience() {
		return this.level.getMaxExperience();
	}

	public void RESET() {
		this.permanence.RESET();
	}
}
