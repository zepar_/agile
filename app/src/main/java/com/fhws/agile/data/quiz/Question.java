package com.fhws.agile.data.quiz;

public class Question {
	private String question;
	private String[] answers;
	private int[] solutions;
	private QuestionType type;

	public Question(String question, String[] answers, int[] solutions, QuestionType type) {
		this.question = question;
		this.answers = answers;
		this.solutions = solutions;
		this.type = type;
	}

	public String getQuestion() {
		return question;
	}

	public String[] getAnswers() {
		return answers;
	}

	public int[] getSolutions() {
		return solutions;
	}

	public QuestionType getType() {
		return type;
	}

	public enum QuestionType {
		SINGLE_ANSWER,
		MULTIPLE_ANSWERS
	}
}
