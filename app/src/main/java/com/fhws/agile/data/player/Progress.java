package com.fhws.agile.data.player;

import android.content.Context;
import android.content.res.AssetManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Progress {
	private int progress;
	private int maxProgress;
	private Stage[] stages;

	public Progress(int initProgress, Context context) {
		this.progress = initProgress;

		AssetManager assetManager = context.getAssets();

		String data = null;

		try {
			InputStream inputStream = assetManager.open("progress" + ".json");
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

			StringBuffer rawData = new StringBuffer();
			String line = "";

			int times = 0;
			while(true) {
				line = bufferedReader.readLine();
				if(line != null) {
					rawData.append(line + "\n");
					times++;
				} else break;
			}

			data = rawData.toString();

			bufferedReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			JSONArray rawArray = new JSONArray(data);

			this.stages = new Stage[rawArray.length()];

			for(int i = 0; i < rawArray.length(); i++) {
				JSONObject rawStage = (JSONObject) rawArray.get(i);

				Stage.StageType type = null;

				int rawType = rawStage.getInt("stageType");
				switch(rawType) {
					case 0:
						type = Stage.StageType.QUIZ;
						break;
					case 1:
						type = Stage.StageType.CONTENT;
						break;
					case 2:
						type = Stage.StageType.LINK;
						break;
				}

				this.stages[i] = new Stage(
						rawStage.getString("name"),
						type,
						rawStage.getString("typeId")
				);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		this.maxProgress = this.stages.length;
	}

	public int getProgress() {
		return progress;
	}

	public int getMaxProgress() {
		return this.maxProgress;
	}

	public void addProgress() {
		this.progress++;
		if(this.progress > maxProgress) this.progress = maxProgress;
	}

	public void setProgress(int i) {
		this.progress = i;
	}

	public Stage[] getStages() {
		return this.stages;
	}
}
