package com.fhws.agile.data.content;

import android.content.Context;
import android.content.res.AssetManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Contents {

	private Content[] contents = null;

	public Contents(String id, Context context) {
		AssetManager assetManager = context.getAssets();

		String data = null;

		try {
			InputStream inputStream = assetManager.open("content/"+ id + ".json");
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

			StringBuffer rawData = new StringBuffer();
			String line = "";

			int times = 0;
			while(true) {
				line = bufferedReader.readLine();
				if(line != null) {
					rawData.append(line + "\n");
					times++;
				} else break;
			}

			data = rawData.toString();

			bufferedReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			JSONArray rawContents = new JSONArray(data);

			this.contents = new Content[rawContents.length()];

			for(int i = 0; i < rawContents.length(); i++) {
				JSONObject currentContent = (JSONObject) rawContents.get(i);

				Content.ContentType type = currentContent.getInt("contentType") == 0 ? Content.ContentType.PDF : Content.ContentType.VIDEO;

				Content content = new Content(
						currentContent.getString("name"),
						currentContent.getString("id"),
						type);

				this.contents[i] = content;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public Content[] getContents() {
		return contents;
	}
}
