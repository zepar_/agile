package com.fhws.agile.data.player;

public class Stage {
	private String name;
	private StageType type;
	private String id;

	public Stage(String name, StageType type, String id) {
		this.name = name;
		this.type = type;
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public StageType getType() {
		return type;
	}

	public void setType(StageType type) {
		this.type = type;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public enum StageType {
		QUIZ,
		CONTENT,
		LINK
	}
}
