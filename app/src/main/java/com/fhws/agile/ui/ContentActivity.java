package com.fhws.agile.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fhws.agile.R;
import com.fhws.agile.data.content.Content;
import com.fhws.agile.data.content.Contents;
import com.fhws.agile.data.player.Player;

public class ContentActivity extends AppCompatActivity {
    public static final String EXTRA_CONTENT_ID = "com.fhws.agile.EXTRA_CONTENT_ID";

    private LinearLayout contentContainer;
    private TextView contentsName;
    private Contents contents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);

        this.contentContainer = findViewById(R.id.content_container);
        this.contentsName = findViewById(R.id.content_name);

        Intent intent = getIntent();
        String contentsId = intent.getStringExtra(MainActivity.EXTRA_ID);
        String contentsName = intent.getStringExtra(MainActivity.EXTRA_CONTENT_NAME);

        this.contents = new Contents(contentsId, getApplicationContext());

        this.contentsName.setText(contentsName);

        Player player = Player.getPlayer(getApplicationContext());
        int currentProgress = player.getCurrentProgress();
        int supposedProgress = intent.getIntExtra(MainActivity.EXTRA_STAGE_NUMBER, 0) + 1;

        if(currentProgress < supposedProgress) {
            player.setProgress(supposedProgress);
            player.giveXP(100);
        }

        this.fillContent();
    }

    private void fillContent() {
        final Content[] contentsContent = this.contents.getContents();

        for(int i = 0; i < contentsContent.length; i++) {
            Button newButton = new Button(getApplicationContext());
            newButton.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            newButton.setText(contentsContent[i].getName());
            newButton.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);

            final int finalI = i;
            newButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = null;
                    if(contentsContent[finalI].getType() == Content.ContentType.PDF) {
                        intent = new Intent(getApplicationContext(), PDFActivity.class);
                    } else {
                        intent = new Intent(getApplicationContext(), VideoActivity.class);
                    }
                    intent.putExtra(EXTRA_CONTENT_ID, contentsContent[finalI].getId());

                    startActivity(intent);
                }
            });

            contentContainer.addView(newButton);
        }
    }
}