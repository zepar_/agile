package com.fhws.agile.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.fhws.agile.R;
import com.fhws.agile.data.player.Player;

public class LevelUpDialogFragment extends DialogFragment {
	@NonNull
	@Override
	public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_level_up, null);

		TextView textView = view.findViewById(R.id.level_up_text);

		String level = Integer.toString(Player.getPlayer(getContext()).getCurrentLevel());

		String previousText = textView.getText().toString();

		String newText = previousText.replaceAll("%1", level);

		textView.setText(newText);

		builder.setView(view);

		builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {

			}
		});

		return builder.create();
	}
}
