package com.fhws.agile.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Space;
import android.widget.TextView;

import com.fhws.agile.R;
import com.fhws.agile.data.player.Player;
import com.fhws.agile.data.quiz.Question;
import com.fhws.agile.data.quiz.Quiz;

import java.util.ArrayList;

public class QuizActivity extends AppCompatActivity {

	private RadioGroup radioGroup;
	private TextView question;
	private Button button;

	private Quiz quiz;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_quiz);

		this.radioGroup = findViewById(R.id.radio_group);
		this.question = findViewById(R.id.question);
		this.button = findViewById(R.id.quiz_button);

		Intent intent = getIntent();
		String quizId = intent.getStringExtra(MainActivity.EXTRA_ID);

		this.quiz = new Quiz(quizId, getApplicationContext());

		if(quiz.getCurrentQuestion().getType() == Question.QuestionType.SINGLE_ANSWER) {
			createRadioButtonList(quiz.getCurrentQuestion());
		} else {
			createCheckboxList(quiz.getCurrentQuestion());
		}
	}

	private void createRadioButtonList(Question question) {
		this.question.setText(question.getQuestion());

		for(int i = 0; i < question.getAnswers().length; i++) {
			RadioButton radioButton = new RadioButton(getApplicationContext());
			radioButton.setText(question.getAnswers()[i]);
			radioButton.setTextAppearance(R.style.TextAppearance_AppCompat_Large);
			radioButton.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);

			this.radioGroup.addView(radioButton);

			if(i != question.getAnswers().length - 1) {
				Space space = new Space(getApplicationContext());
				space.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, convertDPtoPixel(20)));
				this.radioGroup.addView(space);
			}
		}
	}

	private void createCheckboxList(Question question) {
		this.question.setText(question.getQuestion());

		for(int i = 0; i < question.getAnswers().length; i++) {
			CheckBox checkBox = new CheckBox(getApplicationContext());
			checkBox.setText(question.getAnswers()[i]);
			checkBox.setTextAppearance(R.style.TextAppearance_AppCompat_Large);
			checkBox.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);

			this.radioGroup.addView(checkBox);

			if(i != question.getAnswers().length - 1) {
				Space space = new Space(getApplicationContext());
				space.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, convertDPtoPixel(20)));
				this.radioGroup.addView(space);
			}
		}
	}

	public void checkAnswer(View view) {
		int[] index;

		if(quiz.getCurrentQuestion().getType() == Question.QuestionType.SINGLE_ANSWER) {
			int buttonId = radioGroup.getCheckedRadioButtonId();
			RadioButton button = findViewById(buttonId);
			int buttonIndex = radioGroup.indexOfChild(button);
			index = new int[]{buttonIndex};
		} else {
			ArrayList<Integer> answers = new ArrayList<>();
			for(int i = 0; i < radioGroup.getChildCount() / 2; i++) {
				if(((CheckBox) radioGroup.getChildAt(i*2)).isChecked()) {
					answers.add(i);
				}
			}

			index = new int[answers.size()];
			for(int i = 0; i < answers.size(); i++) {
				index[i] = answers.get(i);
			}
		}

		boolean answer = quiz.checkAnswer(index);

		if(answer) {
			correctAnswer();
		} else {
			wrongAnswer();
		}

		this.button.setText(R.string.next_question);
		this.button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				QuizActivity.this.nextQuestion(v);
			}
		});
	}

	public void nextQuestion(View view) {
		boolean nextQuestion = quiz.nextQuestion();

		if(nextQuestion) {
			clearList();
			if(quiz.getCurrentQuestion().getType() == Question.QuestionType.SINGLE_ANSWER) {
				createRadioButtonList(quiz.getCurrentQuestion());
			} else {
				createCheckboxList(quiz.getCurrentQuestion());
			}
		} else {
			Player player = Player.getPlayer(getApplicationContext());
			Intent intent = getIntent();

			int currentProgress = player.getCurrentProgress();
			int supposedProgress = intent.getIntExtra(MainActivity.EXTRA_STAGE_NUMBER, 0) + 1;

			if(currentProgress < supposedProgress) {
				player.setProgress(supposedProgress);
				player.giveXP(100);
			}

			finish();
		}

		this.button.setText(R.string.check_answer);
		this.button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				QuizActivity.this.checkAnswer(v);
			}
		});
	}

	private void correctAnswer() {
		int[] correctAnswers = this.quiz.getCurrentQuestion().getSolutions();

		if(correctAnswers.length == 0) {
			for(int i = 0; i < this.quiz.getCurrentQuestion().getAnswers().length; i++) {
				if(((CompoundButton) radioGroup.getChildAt(i * 2)).isChecked()) {
					((CompoundButton) radioGroup.getChildAt(i * 2)).setButtonTintList(ContextCompat.getColorStateList(getApplicationContext(), android.R.color.holo_green_dark));
				}
			}
		}

		for(int i = 0; i < correctAnswers.length; i++) {
			((CompoundButton) radioGroup.getChildAt(correctAnswers[i]*2)).setButtonTintList(ContextCompat.getColorStateList(getApplicationContext(), android.R.color.holo_green_dark));
		}
	}

	private void wrongAnswer() {
		int[] correctAnswers = this.quiz.getCurrentQuestion().getSolutions();

		ArrayList<Integer> wrongAnswersList = new ArrayList<Integer>();
		for(int i = 0, j = 0; i < quiz.getCurrentQuestion().getAnswers().length; i++) {
			if(j < correctAnswers.length && i == correctAnswers[j]) {
				j++;
				continue;
			} else {
				wrongAnswersList.add(i);
			}
		}
		int[] wrongAnswers = new int[wrongAnswersList.size()];
		for(int i = 0; i < wrongAnswersList.size(); i++) {
			wrongAnswers[i] = wrongAnswersList.get(i);
		}

		for(int i = 0; i < correctAnswers.length; i++) {
			CompoundButton button = (CompoundButton) radioGroup.getChildAt(correctAnswers[i]*2);

			if(button.isChecked()) {
				button.setButtonTintList(ContextCompat.getColorStateList(getApplicationContext(), android.R.color.holo_green_dark));
			} else {
				button.setButtonTintList(ContextCompat.getColorStateList(getApplicationContext(), android.R.color.holo_red_dark));
			}
		}

		for (int i = 0; i < wrongAnswers.length; i++) {
			CompoundButton button = (CompoundButton) radioGroup.getChildAt(wrongAnswers[i]*2);

			if(button.isChecked()) {
				button.setButtonTintList(ContextCompat.getColorStateList(getApplicationContext(), android.R.color.holo_red_dark));
			}
		}
	}

	private void clearList() {
		this.radioGroup.removeAllViews();
	}

	private int convertDPtoPixel(int dp) {
		final float scale = getApplicationContext().getResources().getDisplayMetrics().density;
		return (int) (dp * scale + 0.5f);
	}
}
