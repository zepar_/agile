package com.fhws.agile.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.fhws.agile.R;
import com.github.barteksc.pdfviewer.PDFView;

public class PDFActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pdf);

		PDFView pdfView = findViewById(R.id.pdfView);

		Intent intent = getIntent();
		String id = intent.getStringExtra(ContentActivity.EXTRA_CONTENT_ID);

		pdfView.fromAsset("pdf/" + id + ".pdf").load();
	}
}