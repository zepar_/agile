package com.fhws.agile.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.MediaController;
import android.widget.VideoView;

import com.fhws.agile.R;

public class VideoActivity extends AppCompatActivity {

	private VideoView video;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_video);

		this.video = findViewById(R.id.videoView);

		Intent intent = getIntent();
		String videoId = intent.getStringExtra(ContentActivity.EXTRA_CONTENT_ID);

		MediaController cntrl = new MediaController(this) {
			@Override
			public boolean dispatchKeyEvent(KeyEvent event) {
				if(event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
					((Activity) getContext()).finish();
					return true;
				} else if (event.getAction() == KeyEvent.ACTION_UP) {
					((Activity) getContext()).finish();
					return true;
				}
				return super.dispatchKeyEvent(event);
			}
		};

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
			cntrl.addOnUnhandledKeyEventListener(new View.OnUnhandledKeyEventListener() {
				@Override
				public boolean onUnhandledKeyEvent(View v, KeyEvent event) {
					if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
						((Activity) v.getContext()).finish();
					}

					return false;
				}
			});
		}

		video.setMediaController(cntrl);
		video.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/raw/" + videoId));

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
		hideUI();
		executeDelayed();

		video.start();
	}

	/*@Override
	public void onBackPressed() {
		System.out.println("lol");
		finish();
	}*/

	@Override
	public boolean onTouchEvent(MotionEvent e) {
		executeDelayed();
		return true;
	}

	private void executeDelayed() {
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				hideUI();
			}
		}, 3000);
	}

	private void hideUI() {
		getWindow().getDecorView().setSystemUiVisibility(
				View.SYSTEM_UI_FLAG_LAYOUT_STABLE
						| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
						| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
						| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
						| View.SYSTEM_UI_FLAG_FULLSCREEN
						| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
		);
	}
}