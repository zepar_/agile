package com.fhws.agile.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.fhws.agile.R;
import com.fhws.agile.data.player.Player;
import com.fhws.agile.data.player.Stage;

public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_ID = "com.fhws.agile.QUIZ";
    public static final String EXTRA_TYPE = "com.fhws.agile.TYPE";
    public static final String EXTRA_CONTENT_NAME = "com.fhws.agile.NAME";
    public static final String EXTRA_STAGE_NUMBER = "com.fhws.agile.NUMBER";

    private Player player;

    private TextView levelNumber;
    //private TextView XPNumber;
    private ProgressBar xpBar;
    private ProgressBar progressBar;
    private LinearLayout stageContainer;
    private TextView progressText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.player = Player.getPlayer(getApplicationContext());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.levelNumber = findViewById(R.id.level_number);
        //this.XPNumber = findViewById(R.id.exp_number);
        //this.XPNumber.setText(Integer.toString(player.getExperience()));
        this.xpBar = findViewById(R.id.xpBar);
        this.progressBar = findViewById(R.id.progressBar);
        this.progressText = findViewById(R.id.progressText);

        this.stageContainer = findViewById(R.id.stage_container);
        fillStageContainer();
        setProgress();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        int previousLevel = Integer.parseInt(this.levelNumber.getText().toString());

        setProgress();

        int currentLevel = Integer.parseInt(this.levelNumber.getText().toString());

        if(currentLevel != previousLevel) {
            DialogFragment fragment = new LevelUpDialogFragment();
            fragment.show(getSupportFragmentManager(), "level_up");
        }
    }

    private void fillStageContainer() {
        final Stage[] stages = this.player.getProgress().getStages();

        for(int i = 0; i < stages.length; i++) {
            final Button newButton = new Button(getApplicationContext());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(convertDPtoPixel(300), convertDPtoPixel(400));
            params.gravity = Gravity.CENTER;

            newButton.setLayoutParams(params);

            newButton.setText(stages[i].getName());
            newButton.setTextAppearance(R.style.TextAppearance_AppCompat_Large);

            newButton.setEnabled(false);

            final int finalI = i;
            newButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = null;
                    if(stages[finalI].getType() == Stage.StageType.QUIZ) {
                        intent = new Intent(getApplicationContext(), QuizActivity.class);
                        intent.putExtra(EXTRA_TYPE, Stage.StageType.QUIZ);
                    } else if(stages[finalI].getType() == Stage.StageType.CONTENT){
                        intent = new Intent(getApplicationContext(), ContentActivity.class);
                        intent.putExtra(EXTRA_TYPE, Stage.StageType.CONTENT);
                        intent.putExtra(EXTRA_CONTENT_NAME, stages[finalI].getName());
                    } else {
                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse(stages[finalI].getId()));
                    }
                    intent.putExtra(EXTRA_ID, stages[finalI].getId());
                    intent.putExtra(EXTRA_STAGE_NUMBER, finalI);

                    int LAUNCH = 1;
                    startActivityForResult(intent, LAUNCH);

                    /*addProgress(v);
                    addXP(50);*/
                }
            });

            this.stageContainer.addView(newButton, stageContainer.getChildCount() - 1);
        }
    }

    private Button[] getStages() {
        Button[] stages = new Button[this.stageContainer.getChildCount() - 2];
        int j = 0;

        for(int i = 0; i < this.stageContainer.getChildCount(); i++) {
            if(i == 0) continue;
            if(i == this.stageContainer.getChildCount() - 1) continue;

            stages[j] = (Button) this.stageContainer.getChildAt(i);
            j++;
        }

        return stages;
    }

    private void setProgress() {
        Button[] stages = getStages();

        for(int i = 0; i < stages.length; i++) {
            stages[i].setEnabled(false);
        }
        for(int i = 0; i < this.player.getCurrentProgress() + 1; i++) {
            if(i != this.player.getCurrentProgress()) {
                stages[i].setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.completedStage));
            }
            stages[i].setEnabled(true);
        }

        stages[stages.length - 1].setEnabled(true);

        this.levelNumber.setText(Integer.toString(player.getCurrentLevel()));
        //this.XPNumber.setText(Integer.toString(player.getExperience()));
        this.xpBar.setMax(this.player.getMaxExperience());
        this.xpBar.setProgress(this.player.getExperience());

        this.progressBar.setMax(this.player.getProgress().getMaxProgress() - 1);
        this.progressBar.setProgress(this.player.getCurrentProgress());

        this.progressText.setText(Math.round(this.player.getCurrentProgress() / (float) (this.player.getProgress().getMaxProgress() - 1) * 100) + "%");
    }

    public void RESET(View view) {
        this.player.RESET();
        this.setProgress();
    }

    public void addXP(int amount) {
        this.player.giveXP(amount);
        this.levelNumber.setText(Integer.toString(player.getCurrentLevel()));
        //this.XPNumber.setText(Integer.toString(player.getExperience()));
    }



    private int convertDPtoPixel(int dp) {
        final float scale = getApplicationContext().getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }
}
